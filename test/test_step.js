var infopack = require('infopack');
var generator = require('../');

/**
 * Evalute function will run a single step, see more info: https://gitlab.com/sbplatform/infopack
 */
var testpipe = new infopack.Pipeline([], {
    basePath: './test'
});

testpipe.addStep({
    run: function(pipeline) {
        pipeline.setKey('test', [{ test: 'test väde '}]);
    }
})

testpipe.addStep(generator.step({
    storageKey: 'test',
    mapper: function(list) {
        return {
            test: list.test
        }
    }
}));

testpipe.run()
    .then(function() {
        console.log(testpipe.prettyTable());
    })
    .catch(function(err) {
        console.log(err);
    });
